variable "azurerg5" {
  type        = string
  default = "RG05"
}
variable "location" {
  description = "The location for this Lab environment"
  type        = string
  default = "west europe"
}

variable "environment_tag" {
  type=string
  default = "dev"
  
}